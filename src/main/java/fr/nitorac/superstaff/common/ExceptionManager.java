package fr.nitorac.superstaff.common;

import fr.nitorac.superstaff.bungee.SuperStaffB;
import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.utils.Logger;
import fr.nitorac.superstaff.utils.Utils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ExceptionManager {
    private Throwable throwable;

    public ExceptionManager(Exception exception) {
        this.throwable = exception;
    }

    public ExceptionManager(Throwable throwable) {
        this.throwable = throwable;
    }

    public boolean register(boolean bl) {
        File dataFolder = Utils.isBungee ? SuperStaffB.getMe().getDataFolder() : SuperStaffP.getMe().getDataFolder();
        if (bl) {
            Logger.INFO.log("");
            Logger.WARN.log("&c[<SuperStaff>] An error occured: " + this.throwable.getMessage());
            Logger.WARN.log("[<SuperStaff>] If you have any questions or need help, please contact the developer.");
            Logger.INFO.log("");
        }
        String time = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());
        File file = new File(dataFolder, "reports/" + time + ".txt");
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        StringWriter stringWriter = new StringWriter();
        this.throwable.printStackTrace(new PrintWriter(stringWriter));
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true))) {
            bufferedWriter.write(stringWriter.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}