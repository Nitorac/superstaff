package fr.nitorac.superstaff.common;

public enum SocketEvents {
    HEARTBEAT("hb"),
    BROADCAST("bd"),
    SAVEBINV("svinv"),
    REQUESTBINV("reqinv"),
    SETPINV("setbinv"),
    REQUESTBSTATE("smget"),
    SETBSTATE("statebset"),
    SETPSTATE("smset"),
    ONDISABLEB("ondisb"),
    ONDISABLEP("ondisp");

    private String event;

    SocketEvents(String event){
        this.event = event;
    }

    public String getEventName(){
        return event;
    }

    public static SocketEvents fromName(String name){
        for(SocketEvents s : values()){
            if(s.getEventName().equals(name)){
                return s;
            }
        }
        return null;
    }
}
