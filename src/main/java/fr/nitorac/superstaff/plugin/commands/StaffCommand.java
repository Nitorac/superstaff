package fr.nitorac.superstaff.plugin.commands;

import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class StaffCommand implements TabExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)){
            sender.sendMessage(Utils.getPrefix() + " You are not a player !");
            return true;
        }
        Player p = (Player)sender;
        boolean newState = !SuperStaffP.getModeCoordinator().isActive(p.getUniqueId());
        if(newState){
            SuperStaffP.getModeCoordinator().enable(p);
        }else{
            SuperStaffP.getModeCoordinator().disable(p);
        }

        msgStaff(p, newState);
        return true;
    }

    public static void msgStaff(Player send, boolean newState){
        send.sendMessage(Utils.getPrefix() + ((newState) ? " §6Super§cStaff §aMode enabled" : " §6Super§cStaff §cMode disabled"));
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return null;
    }
}
