package fr.nitorac.superstaff.plugin.managers;

import fr.nitorac.superstaff.plugin.handlers.MinecraftVersion;
import fr.nitorac.superstaff.plugin.nms.ATitleUtils;
import fr.nitorac.superstaff.plugin.nms.INMSUtils;
import fr.nitorac.superstaff.plugin.nms.IParticleSpawner;
import fr.nitorac.superstaff.plugin.nms.IWorldUtils;
import fr.nitorac.superstaff.utils.ReflectionUtils;

public class VersionManager {
    private MinecraftVersion version;
    private ATitleUtils ATitleUtils;
    private INMSUtils INMSUtils;
    private IParticleSpawner IParticleSpawner;
    private IWorldUtils IWorldUtils;

    public VersionManager(MinecraftVersion minecraftVersion) throws ReflectiveOperationException {
        this.version = minecraftVersion;
        this.load();
    }

    private void load() throws ReflectiveOperationException {
        this.ATitleUtils = loadModule("TitleUtils");
        this.INMSUtils = loadModule("NMSUtils");
        this.IParticleSpawner = loadModule("ParticleSpawner");
        this.IWorldUtils = loadModule("WorldUtils");
    }

    private <T> T loadModule(String string) throws ReflectiveOperationException {
        return (T) ReflectionUtils.instantiateObject(Class.forName("fr.nitorac.superstaff.plugin.nms." + this.version.toString() + "." + string), new Object[0]);
    }

    public ATitleUtils getTitleUtils() {
        return this.ATitleUtils;
    }

    public INMSUtils getNMSUtils() {
        return this.INMSUtils;
    }

    public IParticleSpawner getParticleFactory() {
        return this.IParticleSpawner;
    }

    public IWorldUtils getWorldUtils() {
        return this.IWorldUtils;
    }

    public MinecraftVersion getVersion() {
        return this.version;
    }
}
