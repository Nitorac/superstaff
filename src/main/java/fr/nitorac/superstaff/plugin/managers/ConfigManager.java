package fr.nitorac.superstaff.plugin.managers;

import fr.nitorac.superstaff.common.ExceptionManager;
import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.plugin.exceptions.ConfigManagerNotInitialized;
import fr.nitorac.superstaff.plugin.exceptions.InvalidFieldTypeException;
import fr.nitorac.superstaff.utils.ItemBuilder;
import fr.nitorac.superstaff.utils.Utils;
import org.apache.commons.lang.math.RandomUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ConfigManager {
    private static boolean initialized = false;
    private static final String CONFIG_FILE = "config.yml";

    public static void setBoolean(Field field, boolean bl) {
        ConfigManager.checkForException(field, FieldType.BOOLEAN);
        field.setValue(bl);
    }

    public static void setString(Field field, String string) {
        ConfigManager.checkForException(field, FieldType.STRING);
        field.setValue(string);
    }

    public static void setLocation(Field field, Location location) {
        ConfigManager.checkForException(field, FieldType.LOCATION);
        field.setValue((Object)location);
    }

    public static void setItemStack(Field field, ItemStack itemStack) {
        ConfigManager.checkForException(field, FieldType.ITEMSTACK);
        field.setValue((Object)itemStack);
    }

    public static void addLocation(Field field, Location location) {
        ConfigManager.checkForException(field, FieldType.LOCATION_LIST);
        List<Location> list = ConfigManager.getLocations(field);
        list.add(location);
        field.setValue(list);
    }

    public static void clearLocations(Field field) {
        ConfigManager.checkForException(field, FieldType.LOCATION_LIST);
        List<Location> list = ConfigManager.getLocations(field);
        list.clear();
        field.setValue(list);
    }

    public static String getString(Field field) {
        ConfigManager.checkForException(field, FieldType.STRING);
        return (String)field.getValue();
    }

    public static Boolean getBoolean(Field field) {
        ConfigManager.checkForException(field, FieldType.BOOLEAN);
        return (Boolean)field.getValue();
    }

    public static Integer getInteger(Field field) {
        ConfigManager.checkForException(field, FieldType.INT);
        return (Integer)field.getValue();
    }

    public static Integer getInt(Field field) {
        return ConfigManager.getInteger(field);
    }

    public static Double getDouble(Field field) {
        ConfigManager.checkForException(field, FieldType.DOUBLE);
        return (Double)field.getValue();
    }

    public static ItemStack getItemStack(Field field) {
        ConfigManager.checkForException(field, FieldType.ITEMSTACK);
        return (ItemStack)field.getValue();
    }

    public static Material getMaterial(Field field) {
        ConfigManager.checkForException(field, FieldType.MATERIAL);
        return (Material)field.getValue();
    }

    public static Location getLocation(Field field) {
        if (field.getType() == FieldType.LOCATION_LIST) {
            return ConfigManager.getRdmLocationFromList(field);
        }
        ConfigManager.checkForException(field, FieldType.LOCATION);
        return (Location)field.getValue();
    }

    public static List<Location> getLocations(Field field) {
        ConfigManager.checkForException(field, FieldType.LOCATION_LIST);
        return (List)field.getValue();
    }

    public static Location getRdmLocationFromList(Field field) {
        ConfigManager.checkForException(field, FieldType.LOCATION_LIST);
        List list = (List)field.getValue();
        return (Location)list.get(RandomUtils.nextInt(list.size()));
    }

    public static ConfigurationSection getSubConfig(Field field){
        ConfigManager.checkForException(field, FieldType.SUBCONFIG);
        return (ConfigurationSection)field.getValue();
    }

    private static void checkForException(Field field, FieldType fieldType) {
        if (!initialized) {
            new ConfigManagerNotInitialized("You can't get any field before the ConfigurationManager class hasn't been initialized first.").printStackTrace();
        }
        if (field.getType() != fieldType) {
            new InvalidFieldTypeException("You can't get the field '" + field.toString() + "' as a/an " + fieldType.toString() + " cause it's an instance of a/an " + field.getType().toString()).printStackTrace();
        }
    }

    public static void initConfig() {
        File file;
        if (!SuperStaffP.getMe().getDataFolder().exists()) {
            SuperStaffP.getMe().getDataFolder().mkdirs();
        }

        if (!(file = new File(SuperStaffP.getMe().getDataFolder(), CONFIG_FILE)).exists()) {
            SuperStaffP.getMe().getLogger().info("Generating configuration file ...");
            try (InputStream inputStream = SuperStaffP.getMe().getResource(CONFIG_FILE)) {
                Files.copy(inputStream, file.toPath());
                Bukkit.getLogger().info("Configuration file was generated with success !");
            } catch (IOException exc) {
                new ExceptionManager(exc).register(true);
                Bukkit.getLogger().warning("Error when generating configuration file !");
            }
        }
        Field.init(file);
        initialized = true;
    }

    public enum Field {
        IS_BUNGEE_ENABLED("enable-bungee-support", FieldType.BOOLEAN, false),
        BUNGEE_PORT("bungee-adress", FieldType.STRING, "127.0.0.1:11107"),
        USE_LOCAL_PORT("use-custom-local-port", FieldType.BOOLEAN, false),
        LOCAL_PORT("custom-local-port", FieldType.INT, 11108),
        MAX_RETRIES("connection-max-retries", FieldType.INT, 5),
        MODULES("item-modules", FieldType.SUBCONFIG, "");

        private String path;
        private FieldType type;
        private Object def;
        private String configName;
        private Object value;

        Field(String string2, FieldType fieldType, Object object) {
            this(string2, fieldType, object, ConfigManager.CONFIG_FILE);
        }

        Field(String string2, FieldType fieldType, Object object, String string3) {
            this.path = string2;
            this.type = fieldType;
            this.def = object;
            this.configName = string3;
        }

        public String getPath() {
            return this.path;
        }

        public FieldType getType() {
            return this.type;
        }

        public Object getValue() {
            return this.value;
        }

        public Object getDefault() {
            return this.def;
        }

        public boolean isFile(File file) {
            return this.configName.equals(file.getName());
        }

        private void setValue(Object object) {
            this.value = object;
        }

        public static void init(File file) {
            YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(file);
            for (Field field : Field.values()) {
                if (!field.isFile(file)) continue;
                switch (field.getType()) {
                    case BOOLEAN: {
                        field.setValue(yamlConfiguration.getBoolean(field.getPath(), (Boolean)field.getDefault()));
                        continue;
                    }
                    case DOUBLE: {
                        field.setValue(yamlConfiguration.getDouble(field.getPath(), (Double) field.getDefault()));
                        continue;
                    }
                    case INT: {
                        field.setValue(yamlConfiguration.getInt(field.getPath(), (Integer) field.getDefault()));
                        continue;
                    }
                    case ITEMSTACK: {
                        String string = yamlConfiguration.getString(field.getPath(), (String)field.getDefault());
                        String[] arrstring = string.split(":");
                        if (arrstring.length > 1) {
                            field.setValue(new ItemBuilder(Material.matchMaterial(arrstring[0])).setData(Byte.parseByte(arrstring[1])).toItemStack());
                            continue;
                        }
                        field.setValue(new ItemBuilder(Material.matchMaterial(arrstring[0])).toItemStack());
                        continue;
                    }
                    case MATERIAL: {
                        field.setValue(Material.matchMaterial(yamlConfiguration.getString(field.getPath(), (String)field.getDefault())));
                        continue;
                    }
                    case STRING: {
                        field.setValue(ChatColor.translateAlternateColorCodes('&', yamlConfiguration.getString(field.getPath(), (String)field.getDefault())));
                        continue;
                    }
                    case LOCATION: {
                        field.setValue(Utils.toLocation(yamlConfiguration.getString(field.getPath(), Utils.toString((Location)field.getDefault()))));
                        continue;
                    }
                    case SUBCONFIG: {
                        field.setValue(yamlConfiguration.getConfigurationSection(field.getPath()));
                        continue;
                    }
                    case LOCATION_LIST: {
                        ConfigurationSection configurationSection = yamlConfiguration.getConfigurationSection(field.getPath());
                        ArrayList<Location> arrayList = new ArrayList<>();
                        if (configurationSection != null) {
                            for (String string : configurationSection.getKeys(false)) {
                                arrayList.add(Utils.toLocation(configurationSection.getString(string)));
                            }
                        }
                        field.setValue(arrayList);
                    }
                }
            }
        }
    }

    private enum FieldType {
        STRING("String"),
        BOOLEAN("Boolean"),
        DOUBLE("Double"),
        INT("Integer"),
        ITEMSTACK("ItemStack"),
        LOCATION("Location"),
        LOCATION_LIST("List of Locations"),
        MATERIAL("Material"),
        SUBCONFIG("SubConfig");

        private String toString;

        FieldType(String string2) {
            this.toString = string2;
        }

        public String toString() {
            return this.toString;
        }
    }

}

