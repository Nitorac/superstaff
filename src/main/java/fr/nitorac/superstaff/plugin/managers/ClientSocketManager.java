package fr.nitorac.superstaff.plugin.managers;

import com.google.gson.JsonObject;
import fr.nitorac.superstaff.common.ExceptionManager;
import fr.nitorac.superstaff.common.SocketEvents;
import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.utils.ItemStackIO;
import fr.nitorac.superstaff.utils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import xyz.farhanfarooqui.JRocket.JRocketClient;

import java.net.MalformedURLException;
import java.util.regex.Pattern;

public class ClientSocketManager {

    private static JRocketClient soc;
    private static int retries;

    private ClientSocketManager(){

    }

    private static void baseListeners(){
        soc.onReceive(SocketEvents.SETPINV, data -> {
            OfflinePlayer p = Bukkit.getOfflinePlayer(data.get("pseudo").getAsString());
            try{
                ItemStackIO.deserialize(data.get("inv").getAsJsonObject(), p, data.get("world").getAsString());
            }catch (InvalidConfigurationException e){
                new ExceptionManager(e).register(true);
            }
        });

        soc.onReceive(SocketEvents.SETPSTATE, data -> {
            Player p = Bukkit.getPlayer(data.get("pseudo").getAsString());
            if(data.get("state").getAsBoolean() && p != null){
                SuperStaffP.getModeCoordinator().enable(p);
            }
        });

        soc.onReceive(SocketEvents.ONDISABLEP, data -> {
            data.get("pseudos").getAsJsonArray().forEach(elem -> {
                try{
                    ItemStackIO.deserialize(elem.getAsJsonObject().get("inv").getAsJsonObject(), Bukkit.getOfflinePlayer(elem.getAsJsonObject().get("pseudo").getAsString()), elem.getAsJsonObject().get("world").getAsString());
                }catch (InvalidConfigurationException e){
                    new ExceptionManager(e).register(true);
                }
            });
            SuperStaffP.disableRefillInvsDone = true;
        });
    }

    public static void initSocket(){
        String adress = ConfigManager.getString(ConfigManager.Field.BUNGEE_PORT);
        String[] splitted = adress.split(":");
        if(splitted.length != 2 || !Pattern.compile("^[0-9]+$").matcher(splitted[1]).matches()){
            new ExceptionManager(new MalformedURLException("Not a valid adress for my socket !")).register(true);
            SuperStaffP.getMe().disablePlugin("Socket connection failed");
            return;
        }

        soc = JRocketClient.prepare(splitted[0], Integer.parseInt(splitted[1]), new JRocketClient.RocketClientListener() {
            @Override
            public void onConnect(JRocketClient rocketClient) {
                Logger.INFO.log("Running on port " + rocketClient.getLocalPort());
                Logger.INFO.log("Connected to Bungee (" + rocketClient.getInetAddress().toString() + ") !");
                rocketClient.send(SocketEvents.HEARTBEAT, new JsonObject());
            }

            @Override
            public void onDisconnect(JRocketClient rocketClient) {
                Logger.WARN.log("Disconnected from Bungee");
            }

            @Override
            public void onConnectFailed(JRocketClient client, Exception e){
                if(retries < ConfigManager.getInteger(ConfigManager.Field.MAX_RETRIES)){
                    retries++;
                    Logger.ERROR.log("Connection to Bungee (" + client.getInetAddress().toString() + ") failed ! Retrying in 5 seconds...");
                    new java.util.Timer().schedule(
                        new java.util.TimerTask() {
                            @Override
                            public void run() {
                                soc.connect();
                            }
                        }, 5000);
                }else{
                    SuperStaffP.getMe().disablePlugin("Can't connect to Bungee, disabling plugin ...");
                }
            }
        });

        soc.connect();
        soc.setDebug(true);
        baseListeners();
    }

    public static JRocketClient getSocket() {
        return soc;
    }
}
