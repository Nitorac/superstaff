package fr.nitorac.superstaff.plugin.managers;

import fr.nitorac.superstaff.plugin.items.IStaffItem;
import fr.nitorac.superstaff.plugin.items.StaffListItem;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ModuleItemsManager {

    private static ConcurrentHashMap<String, IStaffItem> actives;

    public ModuleItemsManager(){
        actives = new ConcurrentHashMap<>();
        loadConfig();
    }

    public void loadConfig(){
        ConfigurationSection config = ConfigManager.getSubConfig(ConfigManager.Field.MODULES);
        Set<String> mod = config.getKeys(false);
        mod.forEach(key -> {
            ConfigurationSection in = config.getConfigurationSection(key);
            IStaffItem item = null;
            if(in.getBoolean("enabled")){
                switch(key){
                    case "StaffList":
                        item = new StaffListItem(in);
                        break;
                }
                actives.put(key, item);
            }
        });
    }

    public void setInventory(Player p){
        actives.values().forEach(staffItem -> {
            p.getInventory().setItem(staffItem.getSlot(), staffItem.getItemStack());
        });
    }

    public IStaffItem getStaffItemFromStack(ItemStack stack){
        return actives.values().stream().filter(staffItem -> staffItem.getItemStack().isSimilar(stack)).findFirst().orElse(null);
    }

    public boolean hasStaffInventory(Player p){
        return actives.values().stream().allMatch(staffItem -> p.getInventory().getItem(staffItem.getSlot()).isSimilar(staffItem.getItemStack()));
    }
}
