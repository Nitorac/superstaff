package fr.nitorac.superstaff.plugin;

import fr.nitorac.superstaff.common.ExceptionManager;
import fr.nitorac.superstaff.plugin.commands.StaffCommand;
import fr.nitorac.superstaff.plugin.events.ConnectionEvents;
import fr.nitorac.superstaff.plugin.events.PlayerEvents;
import fr.nitorac.superstaff.plugin.handlers.MinecraftVersion;
import fr.nitorac.superstaff.plugin.handlers.ModeCoordinator;
import fr.nitorac.superstaff.plugin.managers.ClientSocketManager;
import fr.nitorac.superstaff.plugin.managers.ConfigManager;
import fr.nitorac.superstaff.plugin.managers.ModuleItemsManager;
import fr.nitorac.superstaff.plugin.managers.VersionManager;
import fr.nitorac.superstaff.utils.Logger;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public final class SuperStaffP extends JavaPlugin {

    private static SuperStaffP instance;
    private static VersionManager versionManager;
    private static ModeCoordinator modeCoordinator;
    private static ModuleItemsManager moduleItemsManager;

    private Boolean enablePlugin = true;
    private String disableMessage = "";


    public static boolean disableRefillInvsDone = false;

    public SuperStaffP(){
        instance = this;
    }

    @Override
    public void onLoad(){
        MinecraftVersion minecraftVersion = MinecraftVersion.getVersion();
        ArrayList<MinecraftVersion> arrayList = new ArrayList<>();
        arrayList.add(MinecraftVersion.v1_8_R3);
        arrayList.add(MinecraftVersion.v1_8_R4);
        /*arrayList.add(MinecraftVersion.v1_9_R1);
        arrayList.add(MinecraftVersion.v1_9_R2);
        arrayList.add(MinecraftVersion.v1_10_R1);
        arrayList.add(MinecraftVersion.v1_11_R1);
        arrayList.add(MinecraftVersion.v1_12_R1);*/
        if (!minecraftVersion.inRange(arrayList)) {
            this.disablePluginLater("Your Minecraft version is not supported (" + minecraftVersion.toString().replaceAll("_", ".") + ")");
            return;
        }
        try {
            versionManager = new VersionManager(minecraftVersion);
        }
        catch (ReflectiveOperationException reflectiveOperationException) {
            new ExceptionManager(reflectiveOperationException).register(false);
            this.disablePluginLater("ReflectiveOperationException has occured. Please contact the developer.");
            return;
        }
    }

    @Override
    public void onEnable() {
        if (!isSpigotServer() || !enablePlugin) {
            this.disablePlugin(disableMessage);
            return;
        }
        disableRefillInvsDone = false;
        ConfigManager.initConfig();

        modeCoordinator = new ModeCoordinator();
        getServer().getPluginManager().registerEvents(new ConnectionEvents(), this);
        getServer().getPluginManager().registerEvents(new PlayerEvents(), this);
        getCommand("staff").setExecutor(new StaffCommand());

        if(ConfigManager.getBoolean(ConfigManager.Field.IS_BUNGEE_ENABLED)){
            ClientSocketManager.initSocket();
        }

        moduleItemsManager = new ModuleItemsManager();
    }

    @Override
    public void onDisable() {
        int count = 1;
        int max = 80;
        getModeCoordinator().onDisable();
        while(!disableRefillInvsDone){
            Logger.INFO.log("Waiting for original inventories for players in SuperStaff Mode (" + count + "s / " + max + "s)");
            if(count >= max){
                Logger.ERROR.log("Take too long to save inventories on disable !");
                break;
            }
            try {
                Thread.sleep(1000);
                count++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if(ClientSocketManager.getSocket() != null && ClientSocketManager.getSocket().getSocket() != null && !ClientSocketManager.getSocket().hasDisconnected()){
            ClientSocketManager.getSocket().disconnect();
        }
        versionManager = null;
        modeCoordinator = null;

    }

    public void disablePluginLater(String string) {
        this.enablePlugin = false;
        this.disableMessage = string;
    }

    public Boolean isSpigotServer() {
        try {
            Class.forName("org.bukkit.entity.Player$Spigot");
            return true;
        } catch (ClassNotFoundException e) {
            disableMessage += (disableMessage.isEmpty() ? "" : " AND ") + "You must use a Spigot server or one of its derivatives (PaperMC, ";
            return false;
        }
    }

    public void disablePlugin(String string) {
        this.enablePlugin = false;
        Logger.FATAL.log(string);
        this.getPluginLoader().disablePlugin(this);
    }

    public static VersionManager getVersionManager(){
        return versionManager;
    }

    public static SuperStaffP getMe(){
        return instance;
    }

    public static ModeCoordinator getModeCoordinator(){
        return modeCoordinator;
    }

    public static ModuleItemsManager getModuleItems(){
        return moduleItemsManager;
    }
}