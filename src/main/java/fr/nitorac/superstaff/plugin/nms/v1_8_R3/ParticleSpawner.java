package fr.nitorac.superstaff.plugin.nms.v1_8_R3;

import fr.nitorac.superstaff.common.ExceptionManager;
import fr.nitorac.superstaff.plugin.handlers.Particles;
import fr.nitorac.superstaff.plugin.nms.IParticleSpawner;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ParticleSpawner implements IParticleSpawner {
    private static boolean reportedError = false;

    @Override
    public void playParticles(Particles particles, Location location, Float f, Float f2, Float f3, int n, Float f4, int ... arrn) {
        try {
            PacketPlayOutWorldParticles packetPlayOutWorldParticles = new PacketPlayOutWorldParticles(EnumParticle.valueOf(particles.getString()), true, (float)location.getX(), (float)location.getY(), (float)location.getZ(), f, f2, f3, f4, n, arrn);
            for (OfflinePlayer offlinePlayer : Bukkit.getOfflinePlayers()) {
                if (!offlinePlayer.isOnline() || !(offlinePlayer instanceof CraftPlayer)) continue;
                ((CraftPlayer)offlinePlayer).getHandle().playerConnection.sendPacket(packetPlayOutWorldParticles);
            }
        } catch (Exception exception) {
            if (reportedError){
                new ExceptionManager(exception).register(true);
            }
        }
    }

    @Override
    public void playParticles(Player player, Particles particles, Location location, Float f, Float f2, Float f3, int n, Float f4, int ... arrn) {
        try {
            PacketPlayOutWorldParticles packetPlayOutWorldParticles = new PacketPlayOutWorldParticles(EnumParticle.valueOf(particles.getString()), true, (float)location.getX(), (float)location.getY(), (float)location.getZ(), f, f2, f3, f4, n, arrn);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packetPlayOutWorldParticles);
        } catch (Exception exception) {
            if (reportedError){
                new ExceptionManager(exception).register(true);
            }
        }
    }
}

