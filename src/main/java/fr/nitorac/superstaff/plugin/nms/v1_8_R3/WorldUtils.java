package fr.nitorac.superstaff.plugin.nms.v1_8_R3;

import fr.nitorac.superstaff.plugin.nms.IWorldUtils;
import net.minecraft.server.v1_8_R3.Entity;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class WorldUtils implements IWorldUtils {
    @Override
    public void createExplosion(Player player, Location location, float f) {
        this.createExplosion(player, location.getWorld(), location.getX(), location.getY(), location.getZ(), f, true, false);
    }

    @Override
    public void createExplosion(Player player, Location location, float f, boolean bl) {
        this.createExplosion(player, location.getWorld(), location.getX(), location.getY(), location.getZ(), f, true, bl);
    }

    @Override
    public void createExplosion(Player player, Location location, float f, boolean bl, boolean bl2) {
        this.createExplosion(player, location.getWorld(), location.getX(), location.getY(), location.getZ(), f, bl, bl2);
    }

    @Override
    public void createExplosion(Player player, World world, double d, double d2, double d3, float f, boolean bl, boolean bl2) {
        ((CraftWorld)world).getHandle().createExplosion((Entity)((CraftPlayer)player).getHandle(), d, d2, d3, f, bl2, bl);
    }
}

