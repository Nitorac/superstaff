package fr.nitorac.superstaff.plugin.nms.v1_8_R3.utils;

import net.minecraft.server.v1_8_R3.ChatClickable;
import net.minecraft.server.v1_8_R3.ChatHoverable;
import net.minecraft.server.v1_8_R3.ChatMessage;
import net.minecraft.server.v1_8_R3.ChatModifier;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class SpecialMessage {
    private IChatBaseComponent base;

    public SpecialMessage(String string) {
        this.base = new ChatMessage(string);
    }

    public IChatBaseComponent create() {
        return this.base;
    }

    public void sendToPlayer(Player player) {
        ((CraftPlayer)player).getHandle().sendMessage(this.base);
    }

    public IChatBaseComponent append(String string) {
        return this.base.a(string);
    }

    public IChatBaseComponent setHover(String string, ChatHoverable.EnumHoverAction enumHoverAction, String string2) {
        return this.base.addSibling(new ChatMessage(string).setChatModifier(new ChatModifier().setChatHoverable(new ChatHoverable(enumHoverAction, new ChatMessage(string2)))));
    }

    public IChatBaseComponent setClick(String string, ChatClickable.EnumClickAction enumClickAction, String string2) {
        return this.base.addSibling(new ChatMessage(string).setChatModifier(new ChatModifier().setChatClickable(new ChatClickable(enumClickAction, string2))));
    }
}
