package fr.nitorac.superstaff.plugin.nms;

import com.google.gson.JsonObject;
import fr.nitorac.superstaff.plugin.handlers.InteractiveType;
import org.bukkit.DyeColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

public interface INMSUtils {
    void displayInteractiveText(Player var1, String var2, String var3, String var4, InteractiveType var5, String var6);

    void setKiller(Entity var1, Entity var2);

    void setItemInHand(ItemStack var1, Player var2);

    ItemStack setIllegallyGlowing(ItemStack var1, boolean var2);

    void setUnbreakable(ItemMeta var1, boolean var2);

    void setHealth(LivingEntity var1, Double var2);

    void displayRedScreen(Player var1, boolean var2);

    MaterialData getDye(DyeColor var1);

    void saveOfflinePlayerInv(JsonObject root, OfflinePlayer p, String w);
}


