package fr.nitorac.superstaff.plugin.nms;

import fr.nitorac.superstaff.common.ExceptionManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public abstract class ATitleUtils {
    public abstract void titlePacket(Player var1, Integer var2, Integer var3, Integer var4, String var5, String var6);

    public abstract void tabPacket(Player var1, String var2, String var3);

    public abstract void actionBarPacket(Player var1, String var2);

    public void defaultTitle(Type type, Player player, Object ... arrobject) {
        try {
            switch (type) {
                case ACTION: {
                    this.actionBarPacket(player, (String)arrobject[0]);
                    break;
                }
                case TAB: {
                    this.tabPacket(player, (String)arrobject[0], (String)arrobject[1]);
                    break;
                }
                case TITLE: {
                    this.titlePacket(player, 10, 40, 10, (String)arrobject[0], (String)arrobject[1]);
                }
                default: {
                    break;
                }
            }
        }
        catch (Exception exception) {
            new ExceptionManager(exception).register(true);
        }
    }

    public void broadcastDefaultTitle(Type type, Object ... arrobject) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            this.defaultTitle(type, player, arrobject);
        }
    }

    public enum Type {
        TITLE(0),
        TAB(1),
        ACTION(2);

        private int id;

        Type(int n2) {
            this.id = n2;
        }

        public int getId() {
            return this.id;
        }
    }

}