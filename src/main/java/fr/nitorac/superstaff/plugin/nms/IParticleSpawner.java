package fr.nitorac.superstaff.plugin.nms;

import fr.nitorac.superstaff.plugin.handlers.Particles;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface IParticleSpawner {
    void playParticles(Particles var1, Location var2, Float var3, Float var4, Float var5, int var6, Float var7, int ... var8);
    void playParticles(Player var1, Particles var2, Location var3, Float var4, Float var5, Float var6, int var7, Float var8, int ... var9);
}
