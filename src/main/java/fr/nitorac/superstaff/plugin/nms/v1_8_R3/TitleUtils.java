package fr.nitorac.superstaff.plugin.nms.v1_8_R3;

import java.lang.reflect.Field;

import fr.nitorac.superstaff.plugin.nms.ATitleUtils;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class TitleUtils extends ATitleUtils {
    @Override
    public void titlePacket(Player player, Integer n, Integer n2, Integer n3, String string, String string2) {
        IChatBaseComponent iChatBaseComponent;
        PacketPlayOutTitle packetPlayOutTitle;
        PlayerConnection playerConnection = ((CraftPlayer)player).getHandle().playerConnection;
        PacketPlayOutTitle packetPlayOutTitle2 = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, n, n2, n3);
        playerConnection.sendPacket(packetPlayOutTitle2);
        if (string2 != null) {
            iChatBaseComponent = IChatBaseComponent.ChatSerializer.a(("{\"text\": \"" + string2 + "\"}"));
            packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, iChatBaseComponent);
            playerConnection.sendPacket(packetPlayOutTitle);
        }
        if (string != null) {
            iChatBaseComponent = IChatBaseComponent.ChatSerializer.a(("{\"text\": \"" + string + "\"}"));
            packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, iChatBaseComponent);
            playerConnection.sendPacket(packetPlayOutTitle);
        }
    }

    @Override
    public void tabPacket(Player player, String string, String string2) {
        if (string2 == null) {
            string2 = "";
        }
        if (string == null) {
            string = "";
        }
        IChatBaseComponent iChatBaseComponent = IChatBaseComponent.ChatSerializer.a(("{\"text\": \"" + string2 + "\"}"));
        IChatBaseComponent iChatBaseComponent2 = IChatBaseComponent.ChatSerializer.a(("{\"text\": \"" + string + "\"}"));
        PlayerConnection playerConnection = ((CraftPlayer)player).getHandle().playerConnection;
        PacketPlayOutPlayerListHeaderFooter packetPlayOutPlayerListHeaderFooter = new PacketPlayOutPlayerListHeaderFooter(iChatBaseComponent);
        try {
            try {
                Field field = packetPlayOutPlayerListHeaderFooter.getClass().getDeclaredField("b");
                field.setAccessible(true);
                field.set(packetPlayOutPlayerListHeaderFooter, iChatBaseComponent2);
            }
            catch (Exception exception) {
                exception.printStackTrace();
                playerConnection.sendPacket(packetPlayOutPlayerListHeaderFooter);
            }
        }
        finally {
            playerConnection.sendPacket(packetPlayOutPlayerListHeaderFooter);
        }
    }

    @Override
    public void actionBarPacket(Player player, String string) {
        IChatBaseComponent iChatBaseComponent = IChatBaseComponent.ChatSerializer.a(("{\"text\": \"" + string + "\"}"));
        PacketPlayOutChat packetPlayOutChat = new PacketPlayOutChat(iChatBaseComponent, (byte)2);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packetPlayOutChat);
    }
}

