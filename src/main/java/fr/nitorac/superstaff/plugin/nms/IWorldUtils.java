package fr.nitorac.superstaff.plugin.nms;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public interface IWorldUtils {
    void createExplosion(Player var1, Location var2, float var3);

    void createExplosion(Player var1, Location var2, float var3, boolean var4);

    void createExplosion(Player var1, Location var2, float var3, boolean var4, boolean var5);

    void createExplosion(Player var1, World var2, double var3, double var5, double var7, float var9, boolean var10, boolean var11);
}

