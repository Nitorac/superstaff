package fr.nitorac.superstaff.plugin.nms.v1_8_R3;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fr.nitorac.superstaff.common.ExceptionManager;
import fr.nitorac.superstaff.plugin.handlers.InteractiveType;
import fr.nitorac.superstaff.plugin.nms.INMSUtils;
import fr.nitorac.superstaff.plugin.nms.v1_8_R3.utils.SpecialMessage;
import fr.nitorac.superstaff.utils.ItemStackIO;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Dye;
import org.bukkit.material.MaterialData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

public class NMSUtils implements INMSUtils {

    @Override
    public void displayInteractiveText(Player player, String string, String string2, String string3, InteractiveType interactiveType, String string4) {
        SpecialMessage specialMessage = new SpecialMessage(string);
        if (interactiveType.isClickable()) {
            specialMessage.setClick(string2, ChatClickable.EnumClickAction.valueOf((String)interactiveType.toString()), string4);
        } else if (interactiveType.isHoverable()) {
            specialMessage.setHover(string2, ChatHoverable.EnumHoverAction.valueOf((String)interactiveType.toString()), string4);
        }
        specialMessage.append(string3);
        specialMessage.sendToPlayer(player);
    }

    @Override
    public void setKiller(Entity entity, Entity entity2) {
        ((CraftPlayer)entity).getHandle().killer = ((CraftPlayer)entity2).getHandle();
    }

    @Override
    public void setItemInHand(ItemStack itemStack, Player player) {
        player.getInventory().setItemInHand(itemStack);
    }

    @Override
    public ItemStack setIllegallyGlowing(ItemStack itemStack, boolean bl) {
        net.minecraft.server.v1_8_R3.ItemStack itemStack2 = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nBTTagCompound = null;
        if (!itemStack2.hasTag()) {
            nBTTagCompound = new NBTTagCompound();
            itemStack2.setTag(nBTTagCompound);
        }
        if (nBTTagCompound == null) {
            nBTTagCompound = itemStack2.getTag();
        }
        if (bl) {
            NBTTagList nBTTagList = new NBTTagList();
            nBTTagCompound.set("ench", nBTTagList);
        } else {
            nBTTagCompound.remove("ench");
        }
        itemStack2.setTag(nBTTagCompound);
        return CraftItemStack.asCraftMirror(itemStack2);
    }

    @Override
    public void displayRedScreen(Player player, boolean bl) {
        if (bl) {
            WorldBorder worldBorder = new WorldBorder();
            worldBorder.setSize(1.0);
            worldBorder.setCenter(player.getLocation().getX() + 10000.0, player.getLocation().getZ() + 10000.0);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(new PacketPlayOutWorldBorder(worldBorder, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE));
        } else {
            WorldBorder worldBorder = new WorldBorder();
            worldBorder.setSize(3.0E7);
            worldBorder.setCenter(player.getLocation().getX(), player.getLocation().getZ());
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(new PacketPlayOutWorldBorder(worldBorder, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE));
        }
    }

    @Override
    public void setUnbreakable(ItemMeta itemMeta, boolean bl) {
        itemMeta.spigot().setUnbreakable(bl);
    }

    @Override
    public void setHealth(LivingEntity livingEntity, Double d) {
        livingEntity.setMaxHealth(d);
        if (livingEntity instanceof Player) {
            ((Player)livingEntity).setHealthScaled(false);
        }
        livingEntity.setHealth(d);
    }

    @Override
    public MaterialData getDye(DyeColor dyeColor) {
        return new Dye((int)dyeColor.getDyeData());
    }

    @Override
    public void saveOfflinePlayerInv(JsonObject root, OfflinePlayer p, String w) {
        File playerDat = new File(Bukkit.getServer().getWorld(w).getWorldFolder(), "playerdata" + File.separator + p.getUniqueId().toString() + ".dat");
        try {
            NBTTagList list = new NBTTagList();

            JsonObject contents = root.get("inventory").getAsJsonObject();
            for(Map.Entry<String, JsonElement> entry : contents.entrySet()){
                ItemStack i = ItemStackIO.deserialize(entry.getValue().getAsJsonObject());
                NBTTagCompound compound = new NBTTagCompound();
                net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);
                if(item != null){
                    item.save(compound);
                    compound.setByte("Slot", (byte)Integer.parseInt(entry.getKey()));
                    list.add(compound);
                }
            }

            JsonObject armor = root.get("armor").getAsJsonObject();
            for(Map.Entry<String, JsonElement> entry : armor.entrySet()){
                ItemStack i = ItemStackIO.deserialize(entry.getValue().getAsJsonObject());
                NBTTagCompound compound = new NBTTagCompound();
                net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);
                if(item != null){
                    item.save(compound);
                    compound.setByte("Slot", (byte)(Integer.parseInt(entry.getKey()) + 100));
                    list.add(compound);
                }
            }

            /*if(root.has("extra")){
                JsonObject extra = root.get("extra").getAsJsonObject();
                for(Map.Entry<String, JsonElement> entry : extra.entrySet()){
                    ItemStack i = ItemStackIO.deserialize(entry.getValue().getAsJsonObject());
                    NBTTagCompound compound = new NBTTagCompound();
                    net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);
                    if(item != null){
                        item.save(compound);
                        compound.setByte("Slot", (byte)Integer.parseInt(entry.getKey()));
                        list.add(compound);
                    }
                }
            }*/

            NBTTagCompound compound = NBTCompressedStreamTools.a(new FileInputStream(playerDat));
            compound.set("Inventory", list);
            NBTCompressedStreamTools.a(compound, new FileOutputStream(playerDat));
        } catch (InvalidConfigurationException | IOException e) {
            new ExceptionManager(e).register(true);
        }
    }
}
