package fr.nitorac.superstaff.plugin.events;

import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.plugin.items.IStaffItem;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerEvents implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e){
        if(SuperStaffP.getModeCoordinator().isActive(e.getPlayer().getUniqueId())){
            IStaffItem sItem;
            if((sItem = SuperStaffP.getModuleItems().getStaffItemFromStack(e.getItem())) != null){
                sItem.onInteract(e);
            }
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e){
        if(SuperStaffP.getModeCoordinator().isActive(e.getPlayer().getUniqueId())){
            IStaffItem sItem;
            if((sItem = SuperStaffP.getModuleItems().getStaffItemFromStack(e.getPlayer().getInventory().getItemInHand())) != null){
                sItem.onInteractEntity(e);
            }
        }
    }
}
