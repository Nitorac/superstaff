package fr.nitorac.superstaff.plugin.events;

import com.google.gson.JsonObject;
import fr.nitorac.superstaff.common.SocketEvents;
import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.plugin.managers.ClientSocketManager;
import fr.nitorac.superstaff.plugin.managers.ConfigManager;
import fr.nitorac.superstaff.utils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerConnected(PlayerJoinEvent e){
        if(ConfigManager.getBoolean(ConfigManager.Field.IS_BUNGEE_ENABLED)){
            JsonObject sendable = new JsonObject();
            sendable.addProperty("pseudo", e.getPlayer().getName());
            ClientSocketManager.getSocket().send(SocketEvents.REQUESTBSTATE, sendable);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerQuit(PlayerQuitEvent e){
        Player p = e.getPlayer();
        SuperStaffP.getModeCoordinator().disable(p, false);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onWorldChanged(PlayerChangedWorldEvent e){
        Logger.WARN.log("World Changed !");
        SuperStaffP.getModeCoordinator().disable(Bukkit.getOfflinePlayer(e.getPlayer().getUniqueId()), e.getFrom().getName(), false);
        if(ConfigManager.getBoolean(ConfigManager.Field.IS_BUNGEE_ENABLED)){
            JsonObject sendable = new JsonObject();
            sendable.addProperty("pseudo", e.getPlayer().getName());
            ClientSocketManager.getSocket().send(SocketEvents.REQUESTBSTATE, sendable);
        }
    }
}
