package fr.nitorac.superstaff.plugin.handlers;

import com.google.gson.JsonObject;
import fr.nitorac.superstaff.common.ExceptionManager;
import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.utils.ItemStackIO;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class SpigotStorageHandler extends IStorageHandler {

    private static ConcurrentHashMap<UUID, JsonObject> invs;

    public SpigotStorageHandler(){
        invs = new ConcurrentHashMap<>();
    }

    @Override
    public void storeInventory(Player p) {
        invs.put(p.getUniqueId(), ItemStackIO.serialize(p));
    }

    @Override
    public void retrieveInventory(OfflinePlayer p, String world) {
        JsonObject root = invs.get(p.getUniqueId());
        if(root != null){
            try{
                ItemStackIO.deserialize(root, p, world);
            }catch (InvalidConfigurationException e){
                new ExceptionManager(e).register(true);
            }
        }else{
            new ExceptionManager(new NullPointerException("Can't retrieve player inventory, it wasn't saved before.")).register(true);
        }
    }

    @Override
    public void onDisable(){
        SuperStaffP.getModeCoordinator().getActives().forEach(uuid -> retrieveInventory(Bukkit.getPlayer(uuid)));
        SuperStaffP.disableRefillInvsDone = true;
    }
}
