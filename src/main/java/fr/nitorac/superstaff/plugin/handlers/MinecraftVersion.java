package fr.nitorac.superstaff.plugin.handlers;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.Bukkit;

public enum MinecraftVersion {
    UNKNOWN(-1),
    v1_7_R1(10701),
    v1_7_R2(10702),
    v1_7_R3(10703),
    v1_7_R4(10704),
    v1_8_R1(10801),
    v1_8_R2(10802),
    v1_8_R3(10803),
    v1_8_R4(10804),
    v1_9_R1(10901),
    v1_9_R2(10902),
    v1_10_R1(11001),
    v1_11_R1(11101),
    v1_12_R1(11201),
    v1_13_R1(11301),
    v1_13_R2(11302);

    private int version;

    MinecraftVersion(int n2) {
        this.version = n2;
    }

    public int version() {
        return this.version;
    }

    public boolean olderThan(MinecraftVersion minecraftVersion) {
        return this.version() < minecraftVersion.version();
    }

    public boolean newerThan(MinecraftVersion minecraftVersion) {
        return this.version() >= minecraftVersion.version();
    }

    public boolean inRange(MinecraftVersion minecraftVersion, MinecraftVersion minecraftVersion2) {
        return this.newerThan(minecraftVersion) && this.olderThan(minecraftVersion2);
    }

    public boolean inRange(MinecraftVersion ... arrminecraftVersion) {
        return this.inRange(Arrays.asList(arrminecraftVersion));
    }

    public boolean inRange(List<MinecraftVersion> list) {
        for (MinecraftVersion minecraftVersion : list) {
            if (minecraftVersion.version() != this.version()) continue;
            return true;
        }
        return false;
    }

    public boolean matchesPackageName(String string) {
        return string.toLowerCase().contains(this.name().toLowerCase());
    }

    public static MinecraftVersion getVersion() {
        String string = Bukkit.getServer().getClass().getPackage().getName();
        String string2 = string.substring(string.lastIndexOf(46) + 1) + ".";
        for (MinecraftVersion minecraftVersion : MinecraftVersion.values()) {
            if (!minecraftVersion.matchesPackageName(string2)) continue;
            return minecraftVersion;
        }
        Bukkit.getLogger().log(Level.WARNING, "Failed to find version enum for '%s'", string + "'/'" + string2);
        return UNKNOWN;
    }
}

