package fr.nitorac.superstaff.plugin.handlers;

import com.google.gson.JsonObject;
import fr.nitorac.superstaff.common.SocketEvents;
import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.plugin.managers.ClientSocketManager;
import fr.nitorac.superstaff.plugin.managers.ConfigManager;
import fr.nitorac.superstaff.plugin.task.StaffModeTask;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ModeCoordinator {

    private static IStorageHandler storageHandler;
    private static ConcurrentHashMap<UUID, StaffBundle> bundles;
    private static StaffModeTask staffModeTask;

    public ModeCoordinator(){
        bundles = new ConcurrentHashMap<>();
        storageHandler = (ConfigManager.getBoolean(ConfigManager.Field.IS_BUNGEE_ENABLED)) ? new BungeeStorageHandler() : new SpigotStorageHandler();
        staffModeTask = new StaffModeTask(this);
        staffModeTask.runTaskTimer(SuperStaffP.getMe(), 0L, 5L);
    }

    public boolean enable(Player p){
        return enable(p, true);
    }

    public boolean enable(Player p, boolean sendState){
        if(isActive(p.getUniqueId())){
            return false;
        }

        if(!SuperStaffP.getModuleItems().hasStaffInventory(p)){
            storageHandler.storeInventory(p);
        }

        p.getInventory().clear();
        // Fill items
        SuperStaffP.getModuleItems().setInventory(p);
        // END Fill items
        bundles.put(p.getUniqueId(), getBundle(p.getUniqueId()).setActive(true));
        if(sendState){
            sendState(p.getName(), true);
        }
        return true;
    }

    public boolean disable(Player p){
        return disable(p, true);
    }

    public boolean disable(OfflinePlayer p, String world){
        return disable(p, world, true);
    }

    public boolean disable(Player p, boolean sendState){
        return disable(Bukkit.getOfflinePlayer(p.getUniqueId()), p.getWorld().getName(), sendState);
    }

    public boolean disable(OfflinePlayer p, String world, boolean sendState){
        if(!isActive(p.getUniqueId())){
            return false;
        }

        storageHandler.retrieveInventory(p, world);
        bundles.put(p.getUniqueId(), getBundle(p.getUniqueId()).setActive(false));
        if(sendState){
            sendState(p.getName(), false);
        }
        return true;
    }

    public void onDisable(){
        storageHandler.onDisable();
    }

    private void sendState(String pseudo, boolean state){
        if(ConfigManager.getBoolean(ConfigManager.Field.IS_BUNGEE_ENABLED)){
            JsonObject sendable = new JsonObject();
            sendable.addProperty("pseudo", pseudo);
            sendable.addProperty("state", state);
            ClientSocketManager.getSocket().send(SocketEvents.SETBSTATE, sendable);
        }
    }

    public boolean isActive(UUID uuid){
        return getBundle(uuid).isActive();
    }

    public List<UUID> getActives(){
        return bundles.entrySet().stream().filter(entry -> entry.getValue().isActive()).map(Map.Entry::getKey).collect(Collectors.toList());
    }

    public StaffBundle getBundle(UUID uuid){
        return bundles.getOrDefault(uuid, new StaffBundle());
    }
}
