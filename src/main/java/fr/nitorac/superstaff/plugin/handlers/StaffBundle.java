package fr.nitorac.superstaff.plugin.handlers;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bukkit.World;

@Data
@Accessors(chain = true)
public class StaffBundle {
    private boolean active = false;
    private World currentWorld = null;
}
