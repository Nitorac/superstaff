package fr.nitorac.superstaff.plugin.handlers;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import fr.nitorac.superstaff.common.ExceptionManager;
import fr.nitorac.superstaff.common.SocketEvents;
import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.plugin.managers.ClientSocketManager;
import fr.nitorac.superstaff.utils.ItemStackIO;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.net.SocketException;
import java.util.stream.Collectors;

public class BungeeStorageHandler extends IStorageHandler {

    @Override
    public void storeInventory(Player p) {
        JsonObject sendable = new JsonObject();
        sendable.addProperty("pseudo", p.getName());
        sendable.add("inv", ItemStackIO.serialize(p));
        sendable.addProperty("world", p.getWorld().getName());
        ClientSocketManager.getSocket().send(SocketEvents.SAVEBINV, sendable);
    }

    @Override
    public void retrieveInventory(OfflinePlayer p, String world) {
        JsonObject sendable = new JsonObject();
        sendable.addProperty("pseudo", p.getName());
        sendable.addProperty("world", world);
        ClientSocketManager.getSocket().send(SocketEvents.REQUESTBINV, sendable);
    }

    @Override
    public void onDisable(){
        JsonObject sendable = new JsonObject();
        sendable.add("pseudos", new GsonBuilder().create().toJsonTree(SuperStaffP.getModeCoordinator().getActives().stream().map(uuid -> {
            JsonObject obj = new JsonObject();
            Player p = Bukkit.getPlayer(uuid);
            obj.addProperty("pseudo", p.getName());
            obj.addProperty("world", p.getWorld().getName());
            return obj;
        }).collect(Collectors.toList())));
        if(!ClientSocketManager.getSocket().send(SocketEvents.ONDISABLEB, sendable)){
            new ExceptionManager(new SocketException("Cannot retrieve inventory from Bungee !")).register(true);
            SuperStaffP.disableRefillInvsDone = true;
        }
    }
}
