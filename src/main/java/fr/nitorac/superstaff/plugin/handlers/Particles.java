package fr.nitorac.superstaff.plugin.handlers;

import com.google.common.collect.Maps;
import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.plugin.task.ParticleTask;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.Set;

public enum Particles {
    EXPLOSION_NORMAL("explode", 0, true),
    EXPLOSION_LARGE("largeexplode", 1, true),
    EXPLOSION_HUGE("hugeexplosion", 2, true),
    FIREWORKS_SPARK("fireworksSpark", 3, false),
    WATER_BUBBLE("bubble", 4, false),
    WATER_SPLASH("splash", 5, false),
    WATER_WAKE("wake", 6, false),
    SUSPENDED("suspended", 7, false),
    SUSPENDED_DEPTH("depthsuspend", 8, false),
    CRIT("crit", 9, false),
    CRIT_MAGIC("magicCrit", 10, false),
    SMOKE_NORMAL("smoke", 11, false),
    SMOKE_LARGE("largesmoke", 12, false),
    SPELL("spell", 13, false),
    SPELL_INSTANT("instantSpell", 14, false),
    SPELL_MOB("mobSpell", 15, false),
    SPELL_MOB_AMBIENT("mobSpellAmbient", 16, false),
    SPELL_WITCH("witchMagic", 17, false),
    DRIP_WATER("dripWater", 18, false),
    DRIP_LAVA("dripLava", 19, false),
    VILLAGER_ANGRY("angryVillager", 20, false),
    VILLAGER_HAPPY("happyVillager", 21, false),
    TOWN_AURA("townaura", 22, false),
    NOTE("note", 23, false),
    PORTAL("portal", 24, false),
    ENCHANTMENT_TABLE("enchantmenttable", 25, false),
    FLAME("flame", 26, false),
    LAVA("lava", 27, false),
    FOOTSTEP("footstep", 28, false),
    CLOUD("cloud", 29, false),
    REDSTONE("reddust", 30, false),
    SNOWBALL("snowballpoof", 31, false),
    SNOW_SHOVEL("snowshovel", 32, false),
    SLIME("slime", 33, false),
    HEART("heart", 34, false),
    BARRIER("barrier", 35, false),
    ITEM_CRACK("iconcrack_", 36, false, 2),
    BLOCK_CRACK("blockcrack_", 37, false, 1),
    BLOCK_DUST("blockdust_", 38, false, 1),
    WATER_DROP("droplet", 39, false),
    ITEM_TAKE("take", 40, false),
    MOB_APPEARANCE("mobappearance", 41, true),
    DRAGON_BREATH("dragonbreath", 42, false, MinecraftVersion.v1_9_R1, PORTAL),
    END_ROD("endrod", 43, false, MinecraftVersion.v1_9_R1, FIREWORKS_SPARK),
    DAMAGE_INDICATOR("damageindicator", 44, false, MinecraftVersion.v1_9_R1, TOWN_AURA),
    SWEEP_ATTACK("sweepattack", 45, false, MinecraftVersion.v1_9_R1, CRIT_MAGIC);

    private final String name;
    private final int id;
    private final boolean bool;
    private final int option;
    private final MinecraftVersion supportedVersion;
    private final Particles alternative;
    private static final Map<Integer, Particles> particlesId;
    private static final Map<String, Particles> particlesName;
    private static final MinecraftVersion version;

    static {
        version = SuperStaffP.getVersionManager().getVersion();
        particlesId = Maps.newHashMap();
        particlesName = Maps.newHashMap();
        for (Particles particles : Particles.values()) {
            particlesId.put(particles.getId(), particles);
            particlesName.put(particles.getName(), particles);
        }
    }

    Particles(String string2, int n2, boolean bl, int n3, MinecraftVersion minecraftVersion, Particles particles) {
        this.supportedVersion = minecraftVersion;
        this.alternative = particles;
        this.name = string2;
        this.id = n2;
        this.bool = bl;
        this.option = n3;
    }

    Particles(String string2, int n2, boolean bl, MinecraftVersion minecraftVersion, Particles particles) {
        this(string2, n2, bl, 0, minecraftVersion, particles);
    }

    Particles(String string2, int n2, boolean bl, int n3) {
        this(string2, n2, bl, n3, MinecraftVersion.v1_8_R1, null);
    }

    Particles(String string2, int n2, boolean bl) {
        this(string2, n2, bl, 0, MinecraftVersion.v1_8_R1, null);
    }

    public static Set<String> getParticles() {
        return particlesName.keySet();
    }

    public String getName() {
        return this.name;
    }

    public String getString() {
        return version.newerThan(this.supportedVersion) ? this.toString() : (this.alternative == null ? TOWN_AURA.toString() : this.alternative.toString());
    }

    public int getId() {
        return version.newerThan(this.supportedVersion) ? this.id : (this.alternative == null ? TOWN_AURA.getId() : this.alternative.getId());
    }

    public int getOptions() {
        return this.option;
    }

    public MinecraftVersion getVersion() {
        return this.supportedVersion;
    }

    public Particles getAlternative() {
        return this.alternative;
    }

    public boolean getBoolean() {
        return this.bool;
    }

    public static Particles getEnumById(int n) {
        return particlesId.get(n);
    }

    public static Particles getEnumByString(String string) {
        return particlesName.get(string);
    }

    public enum ParticleEffect {
        /*FLAME_RINGS(new FlameRingsEffect(), 0),
        FLAME_CIRCLE(new FlameCircleEffect(), 0),
        FLAME_SPIRAL(new FlameSpiralEffect(), 0),
        WITCH_SPIRAL(new WitchSpiralEffect(), 0),
        ENCHANT(new EnchantEffect(), 5),*/
        RANDOM_FILLER((var1, var2) -> {}, 8);

        private ParticleEffectType action;
        private int ticks;

        ParticleEffect(ParticleEffectType particleEffectType, int n2) {
            this.action = particleEffectType;
            this.ticks = n2;
        }

        public ParticleEffectType getAction() {
            return this.action;
        }

        public int getTicks() {
            return this.ticks;
        }

        public static void equipEffect(Player player, ParticleEffect particleEffect) {
            new ParticleTask(particleEffect, player);
        }

        public interface ParticleEffectType {
            void update(Player var1, Boolean var2);
        }
    }
}