package fr.nitorac.superstaff.plugin.handlers;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public abstract class IStorageHandler {
    public abstract void storeInventory(Player p);

    public abstract void retrieveInventory(OfflinePlayer p, String world);

    public void retrieveInventory(Player p){
        retrieveInventory(Bukkit.getOfflinePlayer(p.getUniqueId()), p.getWorld().getName());
    }

    public abstract void onDisable();
}
