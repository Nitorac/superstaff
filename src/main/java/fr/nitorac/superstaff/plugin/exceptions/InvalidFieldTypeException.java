package fr.nitorac.superstaff.plugin.exceptions;

public class InvalidFieldTypeException extends Exception {

    public InvalidFieldTypeException(String string) {
        super(string);
    }
}

