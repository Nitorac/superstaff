package fr.nitorac.superstaff.plugin.exceptions;

public class ConfigManagerNotInitialized extends Exception {

    public ConfigManagerNotInitialized(String msg){
        super(msg);
    }
}
