package fr.nitorac.superstaff.plugin.items;

import fr.nitorac.superstaff.plugin.items.logic.StaffListLogic;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class StaffListItem extends IStaffItem{

    private StaffListLogic logic;

    public StaffListItem(ConfigurationSection in){
        super(in);
        logic = new StaffListLogic();
    }

    @Override
    public void onInteract(PlayerInteractEvent e) {
        e.getPlayer().sendMessage("Item utilisé !    " + e.getAction().name());
    }

    @Override
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        e.getPlayer().sendMessage("Item utilisé sur entité !");
    }
}
