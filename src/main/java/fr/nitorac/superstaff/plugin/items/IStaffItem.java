package fr.nitorac.superstaff.plugin.items;

import fr.nitorac.superstaff.utils.Utils;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.stream.Collectors;

public abstract class IStaffItem {

    private int slot;
    private ItemStack itemStack;
    private ConfigurationSection params;

    public IStaffItem(ConfigurationSection in){
        this.slot = in.getInt("slot");
        itemStack = prepareItem(new ItemStack(Material.getMaterial(in.getString("item-material")), 1, (short)in.getInt("item-damage")), in.getString("item-name"), in.getStringList("item-lore"));
        this.params = in;
    }

    public abstract void onInteract(PlayerInteractEvent e);
    public abstract void onInteractEntity(PlayerInteractEntityEvent e);

    protected ItemStack prepareItem(ItemStack stack, String name, List<String> lore){
        ItemMeta meta = stack.getItemMeta();
        meta.spigot().setUnbreakable(true);
        meta.setDisplayName(Utils.format(name));
        meta.setLore(lore.stream().map(Utils::format).collect(Collectors.toList()));
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        stack.setItemMeta(meta);
        return stack;
    }

    public int getSlot(){
        return slot;
    }

    public ItemStack getItemStack(){
        return itemStack;
    }
}
