package fr.nitorac.superstaff.plugin.task;

import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.plugin.handlers.ModeCoordinator;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.UUID;

public class StaffModeTask extends BukkitRunnable {

    private ModeCoordinator mc;

    public StaffModeTask(ModeCoordinator mc){
        this.mc = mc;
    }

    @Override
    public void run() {
        List<UUID> uuids = mc.getActives();
        uuids.forEach(uuid -> {
            Player p = SuperStaffP.getMe().getServer().getPlayer(uuid);

            SuperStaffP.getVersionManager().getTitleUtils().actionBarPacket(p, "§6Super§cStaff §2Mode");
        });
    }
}
