package fr.nitorac.superstaff.plugin.task;

import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.plugin.handlers.Particles;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class ParticleTask
    extends BukkitRunnable {
    private static HashMap<Player, ParticleTask> map = new HashMap<>();
    private Particles.ParticleEffect effect;
    private Player player;
    private boolean isMoving;

    public ParticleTask(Particles.ParticleEffect particleEffect, Player player) {
        if (!map.containsKey(player)) {
            this.isMoving = false;
            this.effect = particleEffect;
            this.player = player;
            map.put(player, this);
            this.runTaskTimer(SuperStaffP.getMe(), 0L, (long)particleEffect.getTicks());
        }
    }

    public void run() {
        if (this.player.isOnline()) {
            if (this.player.getGameMode() != GameMode.SPECTATOR) {
                this.effect.getAction().update(this.player, this.isMoving);
            }
        } else {
            this.cancel();
        }
        this.isMoving = false;
    }

    public static void stop(Player player) {
        if (map.containsKey(player)) {
            map.get(player).cancel();
        }
    }

    public static void move(Player player) {
        if (map.containsKey(player)) {
            ParticleTask.map.get(player).isMoving = true;
        }
    }
}

