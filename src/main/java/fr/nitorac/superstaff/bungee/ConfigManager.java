package fr.nitorac.superstaff.bungee;

import fr.nitorac.superstaff.common.ExceptionManager;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class ConfigManager {

    private static File CONFIG;
    private static Configuration config;

    public ConfigManager(){
        CONFIG = new File(SuperStaffB.getMe().getDataFolder(), "config.yml");

        if(!CONFIG.getParentFile().exists()){
            CONFIG.getParentFile().mkdirs();
        }

        if (!CONFIG.exists()) {
            try (InputStream in = SuperStaffB.getMe().getResourceAsStream("configB.yml")) {
                Files.copy(in, CONFIG.toPath());
            } catch (IOException e) {
                new ExceptionManager(e).register(true);
            }
        }
    }

    public Configuration getConfig(){
        if(config == null){
            try {
                config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(CONFIG);
            } catch (IOException e) {
                new ExceptionManager(e).register(true);
            }
        }
        return config;
    }
}
