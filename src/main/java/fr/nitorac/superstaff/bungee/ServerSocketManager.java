package fr.nitorac.superstaff.bungee;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.nitorac.superstaff.common.ExceptionManager;
import fr.nitorac.superstaff.common.SocketEvents;
import fr.nitorac.superstaff.utils.Logger;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import xyz.farhanfarooqui.JRocket.JRocketServer;

import java.io.IOException;

public class ServerSocketManager {

    private static JRocketServer soc;

    public static final JsonObject emptyInv = new JsonObject();

    public static void init(int port){
        emptyInv.add("inventory", new JsonObject());
        emptyInv.add("armor", new JsonObject());
        emptyInv.add("extra", new JsonObject());
        try {
            // Start listening to port 1234 and set core thread pool size to 500. Each client requires 2 threads, so we'll be handling 250 clients at a time.
            soc = JRocketServer.listen(port, 500);
            soc.setDebug(true);
            soc.setOnClientConnectListener(client -> Logger.INFO.log("New detected bukkit server on " + client.getLocalAdress()));

            soc.setOnClientDisconnectListener(client -> Logger.INFO.log("Bukkit server disconnected on " + client.getId()));
        } catch (IOException e) {
            new ExceptionManager(e).register(true);
        }

        baseListeners();
    }

    public static void baseListeners(){
        soc.onReceive(SocketEvents.REQUESTBSTATE, (data, client) -> {
            ProxiedPlayer p = SuperStaffB.getMe().getProxy().getPlayer(data.get("pseudo").getAsString());
            JsonObject sendable = new JsonObject();
            sendable.addProperty("pseudo", p.getName());
            sendable.addProperty("state", SuperStaffB.getStates().getOrDefault(p.getUniqueId(), false));
            client.send(SocketEvents.SETPSTATE, sendable);
        });

        soc.onReceive(SocketEvents.SETBSTATE, (data, client) -> {
            ProxiedPlayer p = SuperStaffB.getMe().getProxy().getPlayer(data.get("pseudo").getAsString());
            SuperStaffB.getStates().put(p.getUniqueId(), data.get("state").getAsBoolean());
        });

        soc.onReceive(SocketEvents.SAVEBINV, (data, client) -> {
            ProxiedPlayer p = SuperStaffB.getMe().getProxy().getPlayer(data.get("pseudo").getAsString());
            SuperStaffB.getInventorys().put(p.getUniqueId().toString() + client.getLocalAdress().toString() + data.get("world").getAsString(), data.get("inv").getAsJsonObject());
        });

        soc.onReceive(SocketEvents.REQUESTBINV, (data, client) -> {
            ProxiedPlayer p = SuperStaffB.getMe().getProxy().getPlayer(data.get("pseudo").getAsString());
            JsonObject obj = SuperStaffB.getInventorys().getOrDefault(p.getUniqueId().toString() + client.getLocalAdress().toString() + data.get("world").getAsString(), emptyInv);

            JsonObject sendable = new JsonObject();
            sendable.addProperty("pseudo", p.getName());
            sendable.add("world", data.get("world"));
            sendable.add("inv", obj);
            client.send(SocketEvents.SETPINV, sendable);
        });

        soc.onReceive(SocketEvents.ONDISABLEB, (data, client) -> {
            JsonObject sendable = new JsonObject();
            JsonArray array = new JsonArray();
            data.get("pseudos").getAsJsonArray().forEach(elem -> {
                JsonObject obj = new JsonObject();
                ProxiedPlayer p = SuperStaffB.getMe().getProxy().getPlayer(elem.getAsJsonObject().get("pseudo").getAsString());
                if(p != null){
                    obj.addProperty("pseudo", p.getName());
                    obj.addProperty("world", elem.getAsJsonObject().get("world").getAsString());
                    obj.add("inv", SuperStaffB.getInventorys().getOrDefault(p.getUniqueId().toString() + client.getLocalAdress().toString() + elem.getAsJsonObject().get("world").getAsString(), emptyInv));
                    array.add(obj);
                }
            });
            sendable.add("pseudos", array);
            client.send(SocketEvents.ONDISABLEP, sendable);
        });
    }

    public static JRocketServer getSocket(){
        return soc;
    }
}
