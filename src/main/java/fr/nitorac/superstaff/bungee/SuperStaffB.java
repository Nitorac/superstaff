package fr.nitorac.superstaff.bungee;

import com.google.gson.JsonObject;
import fr.nitorac.superstaff.utils.Utils;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class SuperStaffB extends Plugin {

    private static SuperStaffB instance;
    private static ConcurrentHashMap<UUID, Boolean> states;
    private static ConcurrentHashMap<String, JsonObject> invs;
    private static ConfigManager configManager;

    public SuperStaffB(){
        Utils.isBungee = true;
        instance = this;
        states = new ConcurrentHashMap<>();
        invs = new ConcurrentHashMap<>();
    }

    @Override
    public void onEnable() {
        configManager = new ConfigManager();

        ServerSocketManager.init(getConfigManager().getConfig().getInt("socket-port", 11107));
        getProxy().getPluginManager().registerListener(this, new Events());
    }

    @Override
    public void onDisable(){
        if(ServerSocketManager.getSocket() != null && !ServerSocketManager.getSocket().hasDisconnected()){
            try {
                ServerSocketManager.getSocket().stop();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static SuperStaffB getMe(){
        return instance;
    }

    public static ConfigManager getConfigManager(){
        return configManager;
    }

    public static ConcurrentHashMap<UUID, Boolean> getStates(){
        return states;
    }

    public static ConcurrentHashMap<String, JsonObject> getInventorys(){
        return invs;
    }
}
