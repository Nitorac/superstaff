package fr.nitorac.superstaff.utils;

import fr.nitorac.superstaff.bungee.SuperStaffB;
import fr.nitorac.superstaff.plugin.SuperStaffP;

import java.util.logging.Level;

public enum Logger {
    DEBUG("§f[§6Super§cStaff§f-§bDebug§f]§b", Level.INFO),
    INFO("§f[§6Super§cStaff§f-§aInfo§f]§a", Level.INFO),
    WARN("§f[§6Super§cStaff§f-§6Warn§f]§6", Level.WARNING),
    ERROR("§f[§6Super§cStaff§f-§cError§f]§c", Level.SEVERE),
    FATAL("§f[§6Super§cStaff§f-§4Fatal§f]§4", Level.SEVERE);

    String str;
    Level lvl;

    Logger(String str, Level lvl){
        this.str = str;
        this.lvl = lvl;
    }

    public void log(String msg){
        if(Utils.isBungee){
            SuperStaffB.getMe().getLogger().log(lvl, str + " " + msg);
        }else{
            SuperStaffP.getMe().getLogger().log(lvl, str + " " + msg);
        }
    }
}
