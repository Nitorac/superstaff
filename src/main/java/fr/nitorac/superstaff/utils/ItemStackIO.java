package fr.nitorac.superstaff.utils;

import com.dumptruckman.bukkit.configuration.json.JsonConfiguration;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fr.nitorac.superstaff.plugin.SuperStaffP;
import fr.nitorac.superstaff.plugin.handlers.MinecraftVersion;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Map;

public class ItemStackIO {

    public static JsonObject serialize(ItemStack itemStack) {
        JsonConfiguration json = new JsonConfiguration();
        json.set("itemstack", itemStack);
        return new JsonParser().parse(json.saveToString()).getAsJsonObject().get("itemstack").getAsJsonObject();
    }

    public static ItemStack deserialize(JsonObject obj) throws InvalidConfigurationException {
        JsonConfiguration json = new JsonConfiguration();
        JsonObject root = new JsonObject();
        root.add("itemstack", obj);
        json.loadFromString(root.toString());
        return json.getItemStack("itemstack");
    }

    public static void deserialize(JsonObject root, Player p) throws InvalidConfigurationException {
        deserialize(root, Bukkit.getOfflinePlayer(p.getUniqueId()), p.getWorld().getName());
    }

    public static void deserialize(JsonObject root, OfflinePlayer p, String world) throws InvalidConfigurationException {
        boolean finallyOffline = p.getPlayer() == null;
        if (root.has("inventory")) {
            JsonObject inventory = root.get("inventory").getAsJsonObject();
            ItemStack[] invStacks = null;
            if(!finallyOffline){
                invStacks = new ItemStack[p.getPlayer().getInventory().getContents().length];
                for(Map.Entry<String, JsonElement> entry : inventory.entrySet()){
                    invStacks[Integer.parseInt(entry.getKey())] = deserialize(entry.getValue().getAsJsonObject());
                }
            }

            if(p.getPlayer() != null){
                p.getPlayer().getInventory().setContents(invStacks);
            }else{
                finallyOffline = true;
            }
        }

        if(root.has("armor")){
            JsonObject armor = root.get("armor").getAsJsonObject();
            ItemStack[] armorStacks = null;
            if(p.getPlayer() != null){
                armorStacks = new ItemStack[p.getPlayer().getInventory().getArmorContents().length];
                for(Map.Entry<String, JsonElement> entry : armor.entrySet()){
                    armorStacks[Integer.parseInt(entry.getKey())] = deserialize(entry.getValue().getAsJsonObject());
                }
            }else{
                finallyOffline = true;
            }

            if(p.getPlayer() != null){
                p.getPlayer().getInventory().setArmorContents(armorStacks);
            }else{
                finallyOffline = true;
            }
        }

        if(SuperStaffP.getVersionManager().getVersion().newerThan(MinecraftVersion.v1_9_R1)){
            Boolean test = deserialize19(root, p);
            if(test != null){
                finallyOffline = test;
            }
        }

        if(finallyOffline){
            SuperStaffP.getVersionManager().getNMSUtils().saveOfflinePlayerInv(root, p, world);
        }
    }

    public static JsonObject serialize(Player p){
        JsonObject root = new JsonObject();
        PlayerInventory playerInv = p.getInventory();

        JsonObject inventory = new JsonObject();
        for(int i = 0; i < playerInv.getContents().length; i++){
            ItemStack item = playerInv.getContents()[i];
            if(item != null){
                inventory.add(String.valueOf(i), serialize(item));
            }
        }
        root.add("inventory", inventory);

        JsonObject armor = new JsonObject();
        for(int i = 0; i < playerInv.getArmorContents().length; i++){
            ItemStack item = playerInv.getArmorContents()[i];
            if(item != null){
                armor.add(String.valueOf(i), serialize(item));
            }
        }
        root.add("armor", armor);

        if(SuperStaffP.getVersionManager().getVersion().newerThan(MinecraftVersion.v1_9_R1)){
            serialize19(playerInv, root);
        }

        return root;
    }

    public static void serialize19(PlayerInventory playerInv, JsonObject root){
        JsonObject extra = new JsonObject();
        for(int i = 0; i < playerInv.getExtraContents().length; i++){
            ItemStack item = playerInv.getExtraContents()[i];
            if(item != null){
                extra.add(String.valueOf(i), serialize(item));
            }
        }
        root.add("extra", extra);
    }

    public static Boolean deserialize19(JsonObject root, OfflinePlayer p) throws InvalidConfigurationException {
        if(!root.has("extra")){
            return null;
        }
        JsonObject extra = root.get("extra").getAsJsonObject();
        ItemStack[] extraStacks;
        if(p.getPlayer() != null){
            extraStacks = new ItemStack[p.getPlayer().getInventory().getExtraContents().length];
            for(Map.Entry<String, JsonElement> entry : extra.entrySet()){
                extraStacks[Integer.parseInt(entry.getKey())] = deserialize(entry.getValue().getAsJsonObject());
            }
        }else{
            return true;
        }

        if(p.getPlayer() != null){
            p.getPlayer().getInventory().setExtraContents(extraStacks);
        }else{
            return true;
        }

        return false;
    }
}