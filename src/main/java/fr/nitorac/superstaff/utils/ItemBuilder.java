package fr.nitorac.superstaff.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import fr.nitorac.superstaff.plugin.SuperStaffP;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

public class ItemBuilder {
    private ItemStack is;

    public ItemBuilder() {
        this(Material.STONE, 1);
    }

    public ItemBuilder(Material material) {
        this(material, 1);
    }

    public ItemBuilder(ItemStack itemStack) {
        this.is = itemStack;
    }

    public ItemBuilder(Material material, int n) {
        this.is = new ItemStack(material, n);
    }

    public ItemBuilder(Material material, int n, byte by) {
        this.is = new ItemStack(material, n, (short)by);
    }

    public ItemBuilder setData(byte by) {
        this.is = new MaterialData(this.is.getType(), by).toItemStack(this.is.getAmount());
        return this;
    }

    public ItemBuilder setDurability(short s) {
        this.is.setDurability(s);
        return this;
    }

    public ItemBuilder setName(String string) {
        ItemMeta itemMeta = this.is.getItemMeta();
        itemMeta.setDisplayName(string);
        this.is.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder addUnsafeEnchantment(Enchantment enchantment, int n) {
        this.is.addUnsafeEnchantment(enchantment, n);
        return this;
    }

    public ItemBuilder removeEnchantment(Enchantment enchantment) {
        this.is.removeEnchantment(enchantment);
        return this;
    }

    public ItemBuilder setSkullOwner(String string) {
        try {
            if (this.is.getDurability() != SkullType.PLAYER.ordinal()) {
                this.is = new ItemStack(Material.SKULL_ITEM, this.is.getAmount(), (short)SkullType.PLAYER.ordinal());
            }
            SkullMeta skullMeta = (SkullMeta)this.is.getItemMeta();
            skullMeta.setOwner(string);
            this.is.setItemMeta((ItemMeta)skullMeta);
        }
        catch (ClassCastException classCastException) {
            // empty catch block
        }
        return this;
    }

    public ItemBuilder setSkullTexture(String string) {
        ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, (short)SkullType.PLAYER.ordinal());
        if (string.isEmpty()) {
            return this;
        }
        SkullMeta skullMeta = (SkullMeta)itemStack.getItemMeta();
        GameProfile gameProfile = new GameProfile(UUID.randomUUID(), null);
        byte[] arrby = Base64.encodeBase64((byte[])String.format("{textures:{SKIN:{url:\"%s\"}}}", string).getBytes());
        gameProfile.getProperties().put("textures", new Property("textures", new String(arrby)));
        Field field = null;
        try {
            field = skullMeta.getClass().getDeclaredField("profile");
            field.setAccessible(true);
            field.set((Object)skullMeta, (Object)gameProfile);
        }
        catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException exception) {
            exception.printStackTrace();
        }
        itemStack.setItemMeta((ItemMeta)skullMeta);
        this.is = itemStack;
        return this;
    }

    public ItemBuilder addIllegallyGlow() {
        this.is = SuperStaffP.getVersionManager().getNMSUtils().setIllegallyGlowing(this.is, true);
        return this;
    }

    public ItemBuilder removeIllegallyGlow() {
        this.is = SuperStaffP.getVersionManager().getNMSUtils().setIllegallyGlowing(this.is, false);
        return this;
    }

    public ItemBuilder addEnchant(Enchantment enchantment, int n) {
        ItemMeta itemMeta = this.is.getItemMeta();
        itemMeta.addEnchant(enchantment, n, true);
        this.is.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder addEnchantments(Map<Enchantment, Integer> map) {
        this.is.addEnchantments(map);
        return this;
    }

    public ItemBuilder setUnbreakable() {
        this.is.getItemMeta().spigot().setUnbreakable(true);
        return this;
    }

    public ItemBuilder setLore(String ... arrstring) {
        ItemMeta itemMeta = this.is.getItemMeta();
        itemMeta.setLore(Arrays.asList(arrstring));
        this.is.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder setLore(List<String> list) {
        ItemMeta itemMeta = this.is.getItemMeta();
        itemMeta.setLore(list);
        this.is.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder removeLoreLine(String string) {
        ItemMeta itemMeta = this.is.getItemMeta();
        ArrayList<String> arrayList = new ArrayList<>(itemMeta.getLore());
        if (!arrayList.contains(string)) {
            return this;
        }
        arrayList.remove(string);
        itemMeta.setLore(arrayList);
        this.is.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder removeLoreLine(int n) {
        ItemMeta itemMeta = this.is.getItemMeta();
        ArrayList<String> arrayList = new ArrayList<>(itemMeta.getLore());
        if (n < 0 || n > arrayList.size()) {
            return this;
        }
        arrayList.remove(n);
        itemMeta.setLore(arrayList);
        this.is.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder addLoreLine(String ... arrstring) {
        for (int i = 0; i < arrstring.length; ++i) {
            this.addLoreLine(arrstring[i]);
        }
        return this;
    }

    public ItemBuilder addLoreLine(List<String> list) {
        for (String string : list) {
            this.addLoreLine(string);
        }
        return this;
    }

    public ItemBuilder addLoreLine(String string) {
        ItemMeta itemMeta = this.is.getItemMeta();
        ArrayList<String> arrayList = new ArrayList<>();
        if (itemMeta.hasLore()) {
            arrayList = new ArrayList<>(itemMeta.getLore());
        }
        arrayList.add(string);
        itemMeta.setLore(arrayList);
        this.is.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder addLoreLine(String string, int n) {
        ItemMeta itemMeta = this.is.getItemMeta();
        ArrayList<String> arrayList = new ArrayList<>(itemMeta.getLore());
        arrayList.set(n, string);
        itemMeta.setLore(arrayList);
        this.is.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder setColor(DyeColor dyeColor) {
        Material material = this.is.getType();
        if (material.equals((Object)Material.INK_SACK)) {
            this.is = new ItemStack(material, this.is.getAmount(), (short)dyeColor.getDyeData());
        } else if (Arrays.asList(new Material[]{Material.WOOL, Material.STAINED_GLASS_PANE, Material.STAINED_GLASS}).contains((Object)material)) {
            this.is = new ItemStack(material, this.is.getAmount(), (short)dyeColor.getWoolData());
        }
        return this;
    }

    public ItemBuilder setBannerColor(DyeColor dyeColor) {
        if (this.is.getType() != Material.BANNER) {
            return this;
        }
        BannerMeta bannerMeta = (BannerMeta)this.is.getItemMeta();
        bannerMeta.setBaseColor(dyeColor);
        this.is.setItemMeta(bannerMeta);
        return this;
    }

    public ItemBuilder setLeatherArmorColor(Color color) {
        try {
            LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta)this.is.getItemMeta();
            leatherArmorMeta.setColor(color);
            this.is.setItemMeta(leatherArmorMeta);
        }
        catch (ClassCastException classCastException) {

        }
        return this;
    }

    public ItemStack toItemStack() {
        return this.is;
    }

    public static String translateText(String string, String string2) {
        Character c;
        char[] arrc = string2.toCharArray();
        char[] arrc2 = string.toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        int n = 0;
        if (arrc2[0] == '&' && (c = arrc2[1]).toString().matches("[0-9a-fk-o]")) {
            n = 2;
        }
        int n2 = 0;
        for (int i = n; i < arrc2.length; ++i) {
            stringBuilder.append('&').append(arrc[n2 % arrc.length]);
            if (n != 0) {
                stringBuilder.append('&').append(arrc2[1]);
            }
            stringBuilder.append(arrc2[i]);
            if (Character.isWhitespace(arrc2[i])) continue;
            ++n2;
        }
        return ChatColor.translateAlternateColorCodes('&', stringBuilder.toString());
    }
}
