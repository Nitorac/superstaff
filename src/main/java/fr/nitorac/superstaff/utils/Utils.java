package fr.nitorac.superstaff.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

public class Utils {
    public static final String SUPERSTAFF_TAG = "SuperStaff";

    public static boolean isBungee = false;

    public static boolean itemEquals(ItemStack itemStack, ItemStack itemStack2) {
        return !(itemStack.hasItemMeta() && itemStack2.hasItemMeta() ? !itemStack.getItemMeta().getDisplayName().equals(itemStack2.getItemMeta().getDisplayName()) : !itemStack.getType().equals((Object)itemStack2.getType()));
    }

    public static Location toLocation(String string) {
        String[] arrstring = string.split("_");
        World world = Bukkit.getWorld(arrstring[0]);
        if (world == null || arrstring.length < 6) {
            world = Bukkit.getWorlds().get(0);
        }
        return new Location(world, Double.parseDouble(arrstring[1]), Double.parseDouble(arrstring[2]), Double.parseDouble(arrstring[3]), Float.parseFloat(arrstring[4]), Float.parseFloat(arrstring[5]));
    }

    public static String toString(Location location) {
        World world = location.getWorld();
        return world.getName() + "_" + location.getX() + "_" + location.getY() + "_" + location.getZ() + "_" + location.getYaw() + "_" + location.getPitch();
    }

    public static String format(String in){
        return ChatColor.translateAlternateColorCodes('&', in);
    }

    public static String getPrefix(){
        return "§f[§b◀ §6Super§cStaff §b▶§f]§r";
    }
}
