package xyz.farhanfarooqui.JRocket.ServerListeners;

import com.google.gson.JsonObject;
import xyz.farhanfarooqui.JRocket.Client;

public interface OnReceiveListener {
    void onReceive(JsonObject data, Client client);
}
