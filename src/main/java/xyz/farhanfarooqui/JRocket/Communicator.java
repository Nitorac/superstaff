package xyz.farhanfarooqui.JRocket;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fr.nitorac.superstaff.common.ExceptionManager;
import fr.nitorac.superstaff.common.SocketEvents;
import fr.nitorac.superstaff.utils.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import static xyz.farhanfarooqui.JRocket.Constants.DATA;
import static xyz.farhanfarooqui.JRocket.Constants.EVENT;

/**
 * Communicator class handles all the I/O between the client
 * and the Server.
 */

class Communicator {
    private JRocket mJRocket;
    private Socket mSocket;
    private Receiver mReceiver;
    private Sender mSender;
    private boolean hasRun = false;
    private Client.ClientListener mClientListener;
    private ExecutorService mExecutorService;
    private volatile boolean running;
    private LinkedBlockingQueue<JsonObject> mQueue;

    void setClientListener(Client.ClientListener clientListener) {
        this.mClientListener = clientListener;
    }

    Communicator(JRocket JRocket, Socket socket, ExecutorService executorService) throws IOException {
        mJRocket = JRocket;
        mSocket = socket;
        mExecutorService = executorService;
        mReceiver = new Receiver(socket);
        mSender = new Sender(socket);
        mQueue = new LinkedBlockingQueue<>();
    }

    private JRocket getJRocket() {
        return mJRocket;
    }

    void start() {
        if (!hasRun) {
            try {
                mExecutorService.execute(mSender);
                mExecutorService.execute(mReceiver);
            } catch (RejectedExecutionException e) {
                e.printStackTrace();
                disconnect();
            }
            this.running = true;
            hasRun = true;
        }
    }

    void close() {
        disconnect();
    }

    /**
     * Broadcasts to other clients
     * <br>
     * PS. broadcasts to <b>all of the clients</b>.
     */

    void broadCast(SocketEvents event, JsonObject data) {
        ((JRocketServer) getJRocket()).send(event, data);
    }

    /**
     * Sends data
     */
    void send(SocketEvents event, JsonObject data) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(EVENT, event.getEventName());
            jsonObject.add(DATA, data);
            mQueue.put(jsonObject);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class Sender implements Runnable {

        private Socket socket;
        private OutputStreamWriter outputStreamWriter;

        Sender(Socket socket) throws IOException {
            this.socket = socket;
            outputStreamWriter = new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8);
        }

        @Override
        public void run() {
            while (!socket.isClosed()) {
                try {
                    JsonObject jsonObject = mQueue.poll(mJRocket.getHeartBeatRate(), TimeUnit.MILLISECONDS);

                    if (jsonObject == null) {
                        continue;
                    }

                    outputStreamWriter.write(jsonObject.toString().length());
                    outputStreamWriter.write(jsonObject.toString());
                    outputStreamWriter.flush();
                } catch (IOException e) {
                    try {
                        outputStreamWriter.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    e.printStackTrace();
                    break;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            disconnect();
        }
    }

    private class Receiver implements Runnable {
        private Socket socket;
        private BufferedReader bufferedReader;

        Receiver(Socket socket) throws IOException {
            this.socket = socket;
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }

        @Override
        public void run() {
            StringBuilder stringBuilder = new StringBuilder();
            while (!socket.isClosed()) {
                try {
                    int c;
                    char ch;
                    int length = bufferedReader.read();
                    for (int i = 0; i < length; i++) {
                        c = bufferedReader.read();
                        ch = (char) c;
                        stringBuilder.append(ch);
                    }

                    JsonElement elem = new JsonParser().parse(stringBuilder.toString());
                    if (!elem.isJsonObject()) {
                        continue;
                    }
                    JsonObject jsonObject = elem.getAsJsonObject();
                    SocketEvents event = SocketEvents.fromName(jsonObject.get(EVENT).getAsString());
                    JsonObject data = jsonObject.get(DATA).getAsJsonObject();

                    mClientListener.onEventReceive(getJRocket(), event, data);
                    stringBuilder.setLength(0);
                }catch (SocketException e){
                    if(e.getMessage().equalsIgnoreCase("Connection reset")){
                        try{
                            socket.close();
                        }catch (IOException ex){
                            new ExceptionManager(ex).register(true);
                        }
                    }
                    Logger.WARN.log(e.getLocalizedMessage());
                } catch (IOException e) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    e.printStackTrace();
                    break;
                }
            }
            disconnect();
        }
    }

    private void disconnect() {
        if (running) {
            if (!mSocket.isClosed()) {
                try {
                    mSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            mClientListener.onClientDisconnect(getJRocket());
            running = false;
        }
    }
}
