package xyz.farhanfarooqui.JRocket;

import java.io.IOException;
import java.net.SocketAddress;

interface JRocket {
    SocketAddress getInetAddress();

    boolean hasDisconnected();

    void setDebug(boolean debug);

    int getLocalPort();

    SocketAddress getLocalSocketAddress();

    int getSoTimeout() throws IOException;

    void setHeartBeatRate(int heartBeatRate);

    int getHeartBeatRate();

    void setCoreThreadPoolSize(int coreThreadPoolSize);

    void setMaxThreadPoolSize(int maxThreadPoolSize);
}
