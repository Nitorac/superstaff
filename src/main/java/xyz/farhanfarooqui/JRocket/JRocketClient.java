package xyz.farhanfarooqui.JRocket;

import com.google.gson.JsonObject;
import fr.nitorac.superstaff.common.SocketEvents;
import fr.nitorac.superstaff.plugin.managers.ConfigManager;
import fr.nitorac.superstaff.utils.Logger;
import xyz.farhanfarooqui.JRocket.ClientListeners.OnReceiveListener;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JRocketClient implements JRocket {
    private static JRocketClient mRocketClient;
    private static Socket mSocket;
    private static HashMap<SocketEvents, OnReceiveListener> mEventLists;
    private static Communicator mCommunicator;
    private static boolean mKeepAlive = false;
    private static ExecutorService mExecutorService;
    private RocketClientListener mRocketClientListener;
    private int mHeartBeatRate = 0;
    private String mHost;
    private int mPort;
    private boolean debug;

    private boolean disconnected = false;

    {
        mEventLists = new HashMap<>();
    }

    private static Client.ClientListener clientListener = new Client.ClientListener() {
        @Override
        public void onEventReceive(JRocket JRocket, SocketEvents event, JsonObject data) {
            ((JRocketClient) JRocket).onReceiveEvent(event, data);
        }

        @Override
        public void onClientDisconnect(JRocket JRocket) {
            ((JRocketClient) JRocket).onDisconnect();
        }
    };

    private JRocketClient(String host, int port) {
        this(host, port, null);
    }

    private JRocketClient(String host, int port, RocketClientListener rocketClientListener) {
        this.mHost = host;
        this.mPort = port;
        this.mRocketClientListener = rocketClientListener;
        mExecutorService = Executors.newFixedThreadPool(2);
    }


    /**
     * Connects the client to the server. It does its operations on a separate thread to avoid blocking the
     * main thread. Call {@link #setHeartBeatRate(int)} method before connecting.
     */
    public void connect() {
        Thread thread = new Thread(new ConnectRunnable());
        thread.start();
    }

    private class ConnectRunnable implements Runnable {
        @Override
        public void run() {
            try {
                if(ConfigManager.getBoolean(ConfigManager.Field.USE_LOCAL_PORT)){
                    mSocket = new Socket(mHost, mPort, null, ConfigManager.getInteger(ConfigManager.Field.LOCAL_PORT));
                }else{
                    mSocket = new Socket(mHost, mPort);
                }
                mSocket.setSoTimeout(getHeartBeatRate());
                mCommunicator = new Communicator(mRocketClient, mSocket, mExecutorService);
                mCommunicator.setClientListener(clientListener);
                mCommunicator.start();
                disconnected = false;
                mRocketClient.onConnect();
            } catch (IOException e) {
                e.printStackTrace();
                onConnectFailed(e);
            }
        }
    }

    public void setDebug(boolean debug){
        this.debug = debug;
    }

    /**
     * Prepares the client to connect to the host.
     * Call {@link #connect()} method to connect to the host
     */
    public static JRocketClient prepare(String host, int port) {
        mRocketClient = new JRocketClient(host, port);
        return mRocketClient;
    }

    /**
     * Prepares the client to connect to the host.
     * Call {@link #connect()} method to connect to the host
     */
    public static JRocketClient prepare(String host, int port, RocketClientListener rocketClientListener) {
        mRocketClient = new JRocketClient(host, port, rocketClientListener);
        return mRocketClient;
    }

    /**
     * Called when the connection is successfully established
     */
    private void onConnect() {
        if (mRocketClientListener != null)
            mRocketClientListener.onConnect(mRocketClient);
    }

    /**
     * Called if failed to connect to the server, either because of network error or the server is not listening
     */
    private void onConnectFailed(Exception e) {
        if (mRocketClientListener != null)
            mRocketClientListener.onConnectFailed(mRocketClient, e);
    }

    /**
     * Disconnects the client from the server
     */
    public void disconnect() {
        mCommunicator.close();
    }

    /**
     * Called when the client is disconnected from the server
     */
    private void onDisconnect() {
        if (mRocketClientListener != null && !disconnected) {
            disconnected = true;
            mRocketClientListener.onDisconnect(mRocketClient);
        }
    }

    /**
     * Send data to the server
     */
    public boolean send(SocketEvents event, JsonObject data) {
        if (isConnected()) {
            if(debug){
                Logger.DEBUG.log("Send " + event.name() + "  " + data.toString());
            }
            mCommunicator.send(event, data);
            return true;
        } else {
            return false;
        }
    }

    public boolean broadcast(SocketEvents event, JsonObject data){
        JsonObject root = new JsonObject();
        root.addProperty("event", event.getEventName());
        root.add("data", data);
        return send(SocketEvents.BROADCAST, root);
    }

    /**
     * Create an event receive listener
     */
    public void onReceive(SocketEvents event, OnReceiveListener onReceiveListener) {
        mEventLists.put(event, onReceiveListener);
    }

    /**
     * Called when the client receives an event
     */
    private void onReceiveEvent(SocketEvents event, JsonObject data) {
        if (mEventLists.containsKey(event)) {
            mEventLists.get(event).onReceive(data);
            if(debug){
                Logger.DEBUG.log("Receive " + event.name().toUpperCase() + "  " + data.toString());
            }
        }
    }


    /**
     * Returns true if the client has ever connected to the server
     */
    public boolean hasConnected() {
        return mSocket.isConnected();
    }

    /**
     * Returns true if the client is currently connected to the server
     */
    public boolean isConnected() {
        return mSocket != null && !mSocket.isClosed();
    }

    /**
     * Set the option for Keep-Alive
     */
    public void setKeepAlive(boolean on) throws SocketException {
        mKeepAlive = on;
        mSocket.setKeepAlive(on);
    }

    /**
     * Returns true if Keep-Alive is turned on
     */
    public boolean getKeepAlive() throws SocketException {
        return mSocket.getKeepAlive();
    }

    /**
     * Returns the Local InetAddress of the server
     */
    public InetSocketAddress getLocalAddress() {
        return (InetSocketAddress)mSocket.getLocalSocketAddress();
    }

    /**
     * Returns the port number of the server
     */
    public int getPort() {
        return mSocket.getPort();
    }

    /**
     * Returns the InetAddress of the server
     */
    @Override
    public InetSocketAddress getInetAddress() {
        return (InetSocketAddress)mSocket.getRemoteSocketAddress();
    }

    /**
     * Returns true if the socket is currently closed
     */
    @Override
    public boolean hasDisconnected() {
        return mSocket.isClosed();
    }

    /**
     * Get the heartbeat rate in milliseconds
     */
    public int getHeartBeatRate() {
        return mHeartBeatRate;
    }

    /**
     * Set the heartbeat rate in milliseconds. Client will send a heartbeat to the server in n milliseconds. Must be called before {@link #connect()} method is called.
     */
    public void setHeartBeatRate(int milliseconds) {
        this.mHeartBeatRate = milliseconds;
    }

    /**
     * Ignored on client side
     */
    @Override
    public void setCoreThreadPoolSize(int coreThreadPoolSize) {

    }

    /**
     * Ignored on client side
     */
    @Override
    public void setMaxThreadPoolSize(int maxThreadPoolSize) {

    }

    /**
     * Set the timeout period
     */

    @Override
    public int getLocalPort() {
        return mSocket.getLocalPort();
    }

    @Override
    public SocketAddress getLocalSocketAddress() {
        return mSocket.getLocalSocketAddress();
    }

    @Override
    public int getSoTimeout() throws SocketException {
        return mSocket.getSoTimeout();
    }

    public Socket getSocket(){
        return mSocket;
    }

    public interface RocketClientListener {
        void onConnect(JRocketClient rocketClient);

        void onConnectFailed(JRocketClient rocketClient, Exception e);

        void onDisconnect(JRocketClient rocketClient);
    }


}
