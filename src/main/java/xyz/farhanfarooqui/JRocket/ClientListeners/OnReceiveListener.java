package xyz.farhanfarooqui.JRocket.ClientListeners;

import com.google.gson.JsonObject;

public interface OnReceiveListener {
    void onReceive(JsonObject data);
}
