package xyz.farhanfarooqui.JRocket;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
public class Converter {

    public static JsonObject convertObjectToJSON(Object object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        try {
            return new JsonParser().parse(json).getAsJsonObject();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T convertJSONToObject(JsonObject jsonObject, Class<T> type) {
        Gson gson = new Gson();
        String json = jsonObject.toString();
        if (json.isEmpty()) {
            return null;
        } else {
            return gson.fromJson(json, type);
        }
    }
}
